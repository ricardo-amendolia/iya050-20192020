const fs = require("fs");

const path = __dirname;
const files = fs.readdirSync(path);

let schemas = files
  .filter(f => f != "index.js")
  .map(f => fs.readFileSync(`${path}/${f}`).toString("utf8"));

module.exports = schemas;