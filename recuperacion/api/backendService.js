const { GraphQLServer } = require("graphql-yoga");
const typeDefs = require("./schema");
const resolvers = require("./resolvers");
const PORT = 4991;

const server = new GraphQLServer({ typeDefs, resolvers });
module.exports = server.start(
  {
    port:PORT,
    endpoint: "/graphql",
    playground: "/playground",
  },
  () => console.log(`Server is running on http://localhost:${PORT}/playground`)
);

