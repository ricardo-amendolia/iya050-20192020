const NewsService = require("../service/news.service");

module.exports = {
  Query: {
    getNews: (_, args) => NewsService.getNews(args),
  },
  Mutation: {
    createNews: async (_, { input }) => NewsService.createNews(input),
  },
};