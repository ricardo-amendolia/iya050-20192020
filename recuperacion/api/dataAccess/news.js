//@ts-check
const fetch = require("node-fetch");

const applicationId = process.env.APP_ID || "ricardo.amendolia";
const pairID = process.env.PAIR_ID || "noticias-174d-8f7495a5-115d-35b2-726e178f";

const BASE_URL =
  "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs";

const fetchData = async (key, params = {}) => {
  let url = `${BASE_URL}/${key}`;

  return new Promise((resolve, reject) => {
    //@ts-ignore
    fetch(url, {
      headers: {
        Accept: "application/json",
        "x-application-id": applicationId,
      },
      ...params,
    })
      .then((res) => res.json())
      .then((json) => resolve(json))
      .catch((err) => reject(err));
  });
};

const create = async (value) => {
  const oldNewsFeed = await getAll();
  let newsFeed = [];
  if (!oldNewsFeed){
    newsFeed = [value];
  } else {
    newsFeed = [...oldNewsFeed, value];
  }

  return await fetchData(pairID, {
    method: "PUT",
    body: JSON.stringify(newsFeed),
  });
};

const getOne = (key) => {};

const getAll = async () => {
  const data = await fetchData(pairID);
  //error
  if(data.details == "👹👹👹"){
    return new Error("Error 500");
  }
  return JSON.parse(data.value);
};

module.exports = {
  create,
  getAll
};