const { create, getAll } = require("../dataAccess/news");

//generate 4 random char
const s4 = () =>
  Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);

//generate a random ID
const _createId = () => {
  return `${s4()}-${s4()}${s4()}-${s4()}-${s4()}-${s4()}${s4()}`;
};

const createNews = async (params) => {
  const id = _createId();
  const newNew = { id, ...params };

  //guardar nueva noticia
  await create(newNew);
  return newNew;
};

const getNews= async () => {
    console.log("aalskdñla obteniendo noticias");
  const data = await getAll();
  return data;
};

const getNew = () => {};

module.exports = {
  createNews,
  getNews,
  getNew,
};
