const BASE_URL = "https://localhost:4991";

const GqlFetch = (query, variables = {}) => {
  return new Promise((resolve, reject) => {
    fetch(BASE_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ query, variables }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data.data);
        resolve(data.data);
      })
      .catch((err) => reject(err));
  });
};