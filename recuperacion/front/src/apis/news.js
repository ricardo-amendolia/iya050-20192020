const newsQueries = {
    getAll: async () => {
      const query = `
      {
          getNews{
            id
            title
            imgLink
          }
      }`;
      return await GqlFetch(query);
    },

    createNews: async (input) => {
        const query = `
            mutation($input: newsInput!) {
                createNews(input: $input) {
                id
                title
                imgLink
            }
        }`;
        return await GqlFetch(query, { input });
    },
};

exports.newsQueries = newsQueries; 