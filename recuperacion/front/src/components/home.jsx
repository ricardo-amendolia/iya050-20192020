const { React, ReactDOM, ReactRouterDOM } = window;

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            News: [
                {
                    id: 0,
                    title: "Here comes the money",
                    content: "is not really bad :v",
                    autor: "anonimus",
                    imgLink: "https://i.redd.it/flx7xgarx8o41.jpg",
                    comentsCount: "200",
                    positiveVots: "10",
                    negativeVots: "10"
                }
            ]
        }
    }
  componentDidMount() {
    this.getData();
  }

  async getData() {
    const result = await newsQueries.getAll();
    console.log(result)
    this.setState({ News: result.getNews });
  }
  
    publish(posts, feed) {
        return (
            <div className="soft-ui-post" key={posts.id}>
                <h1>{posts.title}</h1>  
                <img src={posts.imgLink} />
            </div>
        );
    }
    render() {
        const { News } = this.state;
        const { feed } = this.props;
        return (
            <div className="center post-list">
                {News.map(posts => {
                    return (
                        <div className="post-container">
                            {this.publish(posts, feed)}
                        </div>
                    )
                })
                }
            </div>)
    }
}
window.Home = Home;

const newsQueries = {
    getAll: async () => {
      const query = `
      {
          getNews{
            id
            title
            imgLink
          }
      }`;
      return await GqlFetch(query);
    },

    createNews: async (input) => {
        const query = `
            mutation($input: newsInput!) {
                createNews(input: $input) {
                id
                title
                imgLink
            }
        }`;
        return await GqlFetch(query, { input });
    },
};

const BASE_URL = "http://localhost:4991/graphql";

const GqlFetch = (query, variables = {}) => {
  return new Promise((resolve, reject) => {
    const result = fetch(BASE_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify({ query, variables }),
      })
        .then(r => r.json())
        .then((data) => {
            console.log(data.data);
            resolve(data.data);
          })
          .catch((err) => reject(err));
  });
};