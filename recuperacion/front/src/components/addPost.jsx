const { React, ReactDOM, ReactRouterDOM } = window;
const Link = ReactRouterDOM.Link;

class AddPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            titulo: '',
            img: ''
        }
        this.sendMeme = this.sendMeme.bind(this);
    }
    sendMeme(e) {
        e.preventDefault();
        const { img, titulo } = this.state;
        const obj = {
            imgLink:img, title:titulo
        }
        
        newsQueries.createNews(obj);
        this.setState({img: "", titulo: ""});
    }
    render() {
        const { titulo, img } = this.state;
        return (
            <div
                className="center"
            >
                <h2>Crear un nuevo post</h2>
                <form>
                    <input
                        className="input post-title"
                        type="input"
                        placeholder="TITLE*"
                        name="title"
                        id="title"
                        required
                        value={titulo}
                        onChange={({target})=> this.setState({titulo: target.value})}
                    />
                  <input
                        className="input post-url"
                        type="input"
                        placeholder="url*"
                        name="imgLink"
                        id="imgLink"
                        required
                        value={img}
                        onChange={({target})=> this.setState({img: target.value})}
                    />
                    <div style={{ display: "grid", justifyContent: "flex-end" }}>
                        <Link to="/home" type="submit" className="soft-ui-btn" onClick={this.sendMeme}>
                            POST
                        </Link>
                    </div>
                </form>
            </div>
        );
    }
}

window.AddPost = AddPost;

const newsQueries = {
    getAll: async () => {
      const query = `
      {
          getNews{
            id
            title
            imgLink
          }
      }`;
      return await GqlFetch(query);
    },

    createNews: async (input) => {
        console.log(input);
        const query = `mutation createNews($input: newsInput){
            createNews(input: $input) {
            title, imgLink
          }
        }`;
        return await GqlFetch(query, { input });
    },
};


const BASE_URL = "http://localhost:4991/graphql";

const GqlFetch = (query, variables = {}) => {
  return new Promise((resolve, reject) => {
    const result = fetch(BASE_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify({ query, variables }),
      })
        .then(r => r.json())
        .then((data) => {
            console.log(data.data);
            resolve(data.data);
          })
          .catch((err) => reject(err));
  });
};