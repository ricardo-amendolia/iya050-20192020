const { React, ReactDOM, ReactRouterDOM } = window;

const Router = ReactRouterDOM.BrowserRouter;
const Route = ReactRouterDOM.Route;
const IndexRoute = ReactRouterDOM.IndexRoute;
const Link = ReactRouterDOM.Link;
const Switch = ReactRouterDOM.Switch;
// const { BrowserRouter, Switch, Route, Link } = ReactRouterDOM;


const App = () => {
  return (
    <Router>
      <div className="navbar">
              <Link to="/">Home</Link>
              <Link to="/addPost">addPost</Link>
      </div>

        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/addPost" component={window.AddPost} />
          <Route path="/" component={window.Home} />
        </Switch>
    </Router>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
