//@ts-check
const http = require("http");
const url = require("url");
const fs = require("fs");

const PORT = process.env.PORT ? process.env.PORT : 3003;

const getFilePath = path => {
  return __dirname + path;
};

//the routes of the server
const routes = {
  all: (req, res) => {
    //return the index html file
    fs.readFile(getFilePath("/index.html"), (err, content) => {
      console.log(err);

      res.writeHead(200, { "Content-Type": "text/html" });
      res.end(content, "utf-8");
    });
  }
};

const serveFiles = (req, res) => {
  const originalUrl = url.parse(req.url);

  fs.readFile(getFilePath(originalUrl.pathname), (err, content) => {
    if (!err) {
      let contentType = "";
      if (originalUrl.pathname.includes(".js")) {
        contentType = "text/javascript";
      }

      res.writeHead(200, { "Content-Type": contentType });

      res.end(content, "utf-8");
    } else {
      // console.log(err);
    }
  });
};

module.exports = http
  .createServer((req, res) => {
    const originalUrl = url.parse(req.url);

    //test if is file server request
    const regex = /\/src\/.+/gm;
    if (regex.test(originalUrl.pathname)) {
      serveFiles(req, res);
    } else {
      //accept all the routes
      routes.all(req, res);

      // const fn = routes[originalUrl.pathname];

      // if (fn) {
      //   fn(req, res);
      // } else {
      //   res.write("Page not fount");
      //   res.end();
      // }
    }
  })
  .listen(PORT, () => console.log(`http://localhost:${PORT}`));
